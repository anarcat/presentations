---
title: TPA documentation demo
date: 2020-09-29
author: Antoine Beaupré
---

## 5-seconds history

 * [TPA Wiki][] started in ~2013, mostly for the support team
 * really took off in 2019
 * reached 100,000 words in September 2020

[TPA Wiki]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/

## image worth a thousand words?

![Word count over time](images/tpa-wiki-word-count-all.png)

## wat

*Kitty cat, surely you did not do **all** of that on your own?*

## Let's zoom in!

![Word count over time, since 2019](images/tpa-wiki-word-count-2019-now.png)

## wiki structure

 * Markdown
 * "flat" structure (no sub-pages)
 * index creates structure
 * _sidebar.md hack

## wanna see!

![TPA wiki screenshot](images/tpa-wiki-screenshot.png)

## what's inside?

 * **Services** documentation of services like GitLab, Nextcloud, backups...
 * **Howtos**: for sysadmins, cross-service, e.g. RAID, new user
 * **Guides**: for users, conflicts with service, limited, e.g. "my
   email doesn't work?"
 * **Policies**: how we decide stuff
 * **Meetings**: minutes

## How do we document things?

<https://www.divio.com/blog/documentation>

4 types of docs:

 * Tutorial: "I know nothing", "how to cook a cake"
 * How-to: "I know how to cook", "cake caught fire, can i still eat
   it?"
 * References: "How is that built anyways?"
 * Discussion: in-depth, off-topic stuff, "could we rebuild this
   another way? why was it setup like that?"

## Service templates

<https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/template>

 * Tutorial
 * How-to: Pager playbook, Disaster recovery, but usually expands beyond
 * Reference: Installation, SLA, Design, Issues, Monitoring and
   testing, Logs and metrics
 * Discussion: Overview, Goals (Must have, Nice to have, Non-Goals),
   Approvals required, Proposed Solution, Cost, Alternatives considered

## Policies

 * **TPA-RFC-1**: policy; the meta-policy (similar to the network team's)
 * **TPA-RFC-2**: support; how to get help, what's supported, support
   levels
 * **TPA-RFC-3**: tools; limit programming languages, programs in use
   (draft)
 * **TPA-RFC-4**: prometheus disk; bump disk usage (a new expense)
 * **TPA-RFC-5**: GitLab migration; how TPA will use GitLab

## What's left?

Ballpark estimate:

 * Internal: 20 services, 42% complete
 * External: 20 services, ~38% complete (excluding unkonwns)
 * Undocumented: 23 services
 * Total: 63 services, about 20% done

## Reminder

 * Quick question: chat (#tpo-admin on OFTC)
 * Bug reports, feature requests and others: issue tracker (GitLab!)
 * Private question and fallback: email (torproject-admin@tpo)

## Questions?

![Cute image of a cyclist](images/tor_bike.png)

## Copyright

This work is licensed under a [Creative Commons Attribution-ShareAlike
4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/)
