# Presentations

Those are presentations I built for or in Tor.

## SOTO 2022

[slides](https://gitlab.torproject.org/anarcat/presentations/builds/artifacts/main/file/soto-2021/raw-slides.pdf?job=build)

## TPA OKRs 2022 Q1/Q2

[slides](https://gitlab.torproject.org/anarcat/presentations/builds/artifacts/main/file/anarcat-tpa-okrs-2021/raw-slides.pdf?job=build)

Part of the 2022 roadmap, see [this ticket](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40439).

## SOTO 2021

[slides](https://gitlab.torproject.org/anarcat/presentations/builds/artifacts/main/file/soto-2021/raw-slides.pdf?job=build)

## TPA docs demo 2020

[slides](https://gitlab.torproject.org/anarcat/presentations/builds/artifacts/main/file/anarcat-demo-2020/slides.pdf?job=build).

# Tooling

I have used the [ahf onion-tex](https://gitlab.torproject.org/ahf/onion-tex/) backend for the
presentations. Originally, I was hoping to use [pandoc](https://pandoc.org/) and
Markdown as a source document, but [that breaks](https://gitlab.torproject.org/ahf/onion-tex/-/issues/1) somehow.

I wrote a [whole blog post about other options](https://anarc.at/blog/2020-09-30-presentation-tools/) so I won't repeat
that here (*again*).
