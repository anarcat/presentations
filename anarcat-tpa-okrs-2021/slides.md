---
title: TPA OKRs 2022
date: 2021-10-20
author: Antoine Beaupré
titlegraphic: \onionlogocolor
---

# Scope the team's work

TPA stands for TorProject system Administrators and we do this:

 * physical and virtual servers' lifecycle management
 * incident response and disaster recovery planning
 * operating system installs and upgrades
 * supported services management
 * access control
 * documentation, ticketing

# Who are we?

 * anarcat (team lead, senior sysadmin)
 * lavamind (sysadmin/web)
 * kez (sysadmin/web)
 * weasel (sysadmin, volunteer, team founder)
 * ln5 (sysadmin, volunteer)
 * qbi (sysadmin, volunteer)

# Anarcat's role

 * 1:1s and other meeting coordination
 * long term plans and policy design and consultation
 * triage coordination
 * software evaluation
 * coding and automation

# Stakeholders

 * everyone

# TPA / Sysadmin OKRs for 2022 Q1/Q2

 * replaces the roadmap process
 * "next 6 months" here means 2022 Q1/Q2

# Improve mail services

 1. David doesn't complain about "mail getting into spam" anymore
 2. RT is not full of spam
 3. we can deliver and receive mail from state.gov

# Retire old services

 1. SVN is retired and people are happy with the replacement
 2. establish a plan for gitolite/gitweb retirement
 3. retire schleuder in favor of ... official Signal groups?
    ... mailman-pgp? RFC2549 with one-time pads?

# Cleanup and publish the sysadmin code base

 1. sanitize and publish the Puppet git repository
 2. implement basic CI for the Puppet repository and use a MR workflow
 3. deploy dynamic environments on the Puppet server to test new features

# Upgrade to Debian 11 "bullseye"

 1. all machines are upgraded to bullseye
 2. migrate to Prometheus for monitoring (or upgrade to Inciga 2)
 3. upgrade to Mailman 3 or retire it in favor of Discourse (!)

# Provision a new, trusted high performance cluster

 1. establish a new PoP on the US west coast with trusted partners and hardware ($$)
 2. retire moly and move the DNS server to the new cluster
 3. reduce VM deployment time to one hour or less (currently 2 hours)

# Non-objectives

Those things will *not* be done during the specified time frame:

 * LDAP retirement
 * static mirror system retirement
 * new offsite backup server
 * complete email services (e.g. mailboxes)
 * search.tpo/SolR
 * web metrics
 * user survey
 * stop global warming

# Questions?

![Cute image of a cyclist](images/tor_bike.png)

# Copyright

This work is licensed under a [Creative Commons Attribution-ShareAlike
4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/)

Thanks to ahf for the theme and support in building those slides.
