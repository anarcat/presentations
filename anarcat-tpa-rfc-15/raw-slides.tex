%% Copyright (c) 2021 The Tor Project, inc. All rights reserved.
%% Use of this source code is governed by a BSD-style
%% license that can be found in the LICENSE file.
\documentclass[aspectratio=169,12pt]{beamer}

\usepackage[utf8]{inputenc}

\usetheme{onion}

\usepackage[shortlabels]{enumitem}

\setlist[description]{align=parleft, labelwidth=3cm}
\setlist[enumerate]{1., font=\color{OnionDarkPurple}}
\setlist[itemize]{label=$\bullet$, font=\color{OnionDarkPurple}}
\setlist[itemize,2]{label=$\circ$, font=\color{OnionDarkPurple}}

\usepackage{csvsimple}
\usepackage[scale=2]{ccicons}
\usepackage{pgfplots}
\usepackage{xcolor}
\usepackage{booktabs}
\usepackage{fontawesome}


%% We do not set footer number since we async needs to record the slides and we might want to reorder them later.
%% \setbeamertemplate{footline}{\raisebox{5pt}{\makebox[\paperwidth]{\hfill\makebox[20pt]{\color{gray}\scriptsize\insertframenumber}}}\hspace*{5pt}}

\newcommand{\highlight}[1]{%
    \textbf{\alert{#1}}
}

\newcommand{\colorhref}[3][OnionBlack]{%
    \href{#2}{\color{#1}{#3}}
}

\title{Changing Email Services}
\author{Antoine Beaupré}
\date{2022-04-20}
\institute{torproject.org}

\titlegraphic{\onionlogocolor}

\setlength{\parskip}{3mm}

\AtBeginPart{
    \let\insertpartnumber\relax
    \let\partname\relax
    \frame{\partpage}
}

\AtBeginSection{
    \let\insertsectionnumber\relax
    \let\sectionname\relax
    \frame{\sectionpage}
}

\AtBeginSubsection{
    \let\insertsubsectionnumber\relax
    \let\subsectionname\relax
    \frame{\subsectionpage}
}

\begin{document}

%% Title Page.
\begin{frame}[plain,noframenumbering]
    \titlepage
\end{frame}

\begin{frame}
  \frametitle{Improve mail services}

Reminder of the first OKR TPA set for Q1/Q2.

\begin{enumerate}

\item
  David doesn't complain about ``mail getting into spam'' anymore
\item
  RT is not full of spam
\item
  we can deliver and receive mail from state.gov
\end{enumerate}
\end{frame}


\begin{frame}
  \frametitle{Current situation}
  \begin{itemize}
  \item massive delivery problems: gmail, outlook, state.gov
  \item generally poor reputation
  \item no SPF, DKIM, DMARC adoption (except on bridges.tpo)
  \item submission server...
  \item ... but incompatible with Apple Mail or Outlook
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{What it looks like}

  \begin{center}
    \includegraphics[width=0.50\textwidth]{images/architecture-pre.png}
  \end{center}

\end{frame}

\begin{frame}
  \frametitle{Proposed changes}

\begin{itemize}

\item End-to-end deliverability checks
\item DMARC reports analysis
\item IMAP / Webmail server deployment
\item Incoming email filtering
\item New mail exchanger (real TLS certs!)
\item New mail relay (reducing eugeni's role)
\item DKIM / ARC signatures  
\item SPF/DMARC records
\item Puppet refactoring

\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{What that should look like}

  \begin{center}
    \includegraphics[width=0.75\textwidth]{images/architecture-post.png}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Challenges}

\begin{itemize}

\item Naming things is hard
\item Spam filtering is hard
\item Security is hard
\item Puppet is hard
\end{itemize}

AKA Some Things are hard to do. We'll try anyways.  

\end{frame}

\begin{frame}
  \frametitle{Timeline}

\begin{enumerate}
\item Q2 (now-ish): monitoring, refactoring, submission server
  adoption
\item Q3: IMAP / webmail / spam filter deployment, deadline for
  submission
\item Q4: DKIM / ARC signatures, soft SPF record
\item 2023 Q1: hard DMARC, SPF
\end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Personas: Ariel, the fundraiser}

\begin{enumerate}
\item email is mission critical
  \begin{itemize}
\item needs to reach funders and partners
\item heavy CiviCRM user
  \end{itemize}
\item currently using Gmail, no LDAP account
\item \textbf{changes}: new LDAP account, new email server adoption
\item \textit{escape hatch:} possibly sending email from Gmail
  (through submission server), but not supported
\end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Personas: Gary, the support guy}

\begin{enumerate}
\item email is mission critical
  \begin{itemize}
\item eats tickets for breakfast
\item sends Tor bridges over TikTok dances
\item ``there's too much spam''
  \end{itemize}
\item currently using Thunderbird and Riseup, has an LDAP account
\item \textbf{changes}: new email server adoption, Riseup is only for
  personal mail now
\end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Personas: John, the external contractor}

\begin{enumerate}
\item email is not mission critical
\item used to run his own mail server, now on Office 365
\item \textbf{changes}: new email server adoption
\item \textit{escape hatch}: possibly sending email from Outlook
  (through submission server), but not supported
\end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Personas: Nancy, the fancy sysadmin}

\begin{enumerate}
\item email is an annoying reality
\item runs her own mail server on a Commodore 64
\item \textbf{changes}: new email server adoption
\item \textit{escape hatch}: possibly sending email from her Postfix
  server (through submission server), but not supported
\end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Personas: Mallory, the director}

\begin{enumerate}
\item email is mission critical
  \begin{itemize}
\item trouble writing to state.gov from her .torproject.org email
\item expired OpenPGP key
  \end{itemize}
\item uses Apple Mail and webmail to read mail on Gmail
\item \textbf{changes}: LDAP account reset, new email server adoption
\item \textit{escape hatch:} possibly sending email from Gmail
  (through submission server), but not supported
\end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Personas: Orpheus, the developer}

\begin{enumerate}
\item email is just another tool (\texttt{\#grantlife})
\item has an LDAP account and their own OVH VM hosting their email
\item \textbf{changes}: new email server adoption
\item \textit{escape hatch:} nothing. this is what they want.
\end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Personas: Blipblop, the bot}

\begin{enumerate}
\item bridgedb, gettor, nagios, etc
\item no LDAP account, relies on system-level MTA
\item \textbf{changes}: no change, TPA will take care of you
\item \textit{escape hatch:} nothing. they should just keep working.
\end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Alternatives}

\begin{enumerate}
\item no mailboxes: no go for Apple Mail
\item high availability: maybe later
\item partial: too hard
\item status quo: people will stop using @torproject.org
\item end of email: not an option for funders?
\item external hosting
\end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{External hosting}

\begin{enumerate}
\item Greenhost: 1600€/year
\item Riseup
\item Gandi: 480\$-2400\$/year
\item Google: 10,000\$/year
\item Fastmail: 6,000\$/year
\item Mailcow: 480€/year
\item Mailfence: 2,500€/year
\end{enumerate}

... versus doing it ourselves...

\end{frame}

\begin{frame}
  \frametitle{Costs}

  \begin{center}
    \includegraphics[width=0.75\textwidth]{images/costs.png}
  \end{center}

  \begin{itemize}
  \item \textbf{80 days} is 32,000EUR of work at 50EUR/hr
  \item \textbf{Recurring}: 1 day per week or month so 5,000-20,000EUR/year
  \end{itemize}
\end{frame}

\begin{frame}[c, plain, noframenumbering]{}
    \centering

    \vfill

    \huge Questions?

    \vfill

    \normalsize

    \begin{columns}
        \begin{column}{0.60\textwidth}
This work is licensed under a
\href{https://creativecommons.org/licenses/by-sa/4.0/}{Creative Commons
Attribution-ShareAlike 4.0 International License}

Thanks to ahf for the theme and support in building those slides.
\centering
   \vfill
    \ccbysa
    \vfill
  \end{column}

        \begin{column}{0.40\textwidth}
            \begin{center}
                \includegraphics[width=0.95\textwidth]{images/tor_bike.png}
            \end{center}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
